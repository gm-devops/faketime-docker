FROM node:carbon
RUN apt-get update \
    && apt-get install -y --no-install-recommends \
        ca-certificates 
RUN npm install -g nodemon 
RUN echo "America/Sao_Paulo" > /etc/timezone && dpkg-reconfigure -f noninteractive tzdata

RUN git clone https://github.com/wolfcw/libfaketime.git
RUN cd libfaketime/src && make install

ENV LD_PRELOAD /usr/local/lib/faketime/libfaketime.so.1
ENV FAKETIME_NO_CACHE 1
ENV DONT_FAKE_MONOTONIC 1