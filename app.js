(function () {
    const express = require('express')
    const app = express()
    const bodyParser = require('body-parser')
    const rotas = require('./rotas')
  
    const apiPort = process.env.PORT || 3030
  
    app.use(bodyParser.json())
  
    app.use((req, res, next) => {
      let now = new Date
      console.log(`[${now.toISOString().replace('T', ' ').slice(0, 23)}] ${req.method} em ${req.path}. Body: ${JSON.stringify(req.body)}`)
      next()
    })
  
    app.use('/', rotas)
  
    // inicia API de controle  
    app.listen(apiPort, function () {
      console.log('API iniciada na porta', apiPort)
    })
  
  })()