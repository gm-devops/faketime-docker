(() => {
  const express = require('express')
  const router = express.Router()
  
  router
    .get('/', (req, res) => { res.status(200).json('API OK') })
    .get('/data', (req, res) => { res.status(200).json(new Date()) })
    .post('/data/set', (req, res) => {
      try {
        let data = req.body.data 
        if (data){
            let date = new Date(data)
            process.env.FAKETIME = `@${date.getFullYear()}-${date.getMonth()+1}-${date.getDate()} ${date.getHours()}:${date.getMinutes()}:${date.getSeconds()}`
            res.status(200).json(`Data alterada para ${new Date()}.`)
        }
        else 
            res.status(404).json(`parâmentro 'data' deve ser não nulo`)
      } catch(e) {
          res.status(500).json(`Erro: ${e}`)
      }
    })
    .post('/data/offset', (req, res) => {
      try {
        let offset = req.body.offset || '+0'
        process.env.FAKETIME = offset;
        res.status(200).json(`Data deslocada em ${offset}. Data atual: ${new Date()}`)
      } catch(e) {
          res.status(500).json(`Erro: ${e}`)
      }
    })
    
  module.exports = router

})()